<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'LesPf Calendar',
    'description' => 'Calendar widget with holidays',
    'category' => 'plugin',
    'author' => 'Peter Willadt',
    'author_email' => 'willadt@t-online.de',
    'state' => 'stable',
    'clearCacheOnLoad' => 1,
    'version' => '0.8.1',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-12.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
