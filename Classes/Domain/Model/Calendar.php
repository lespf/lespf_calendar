<?php

    declare(strict_types=1);

/**
 * This file is part of the "LesPf Calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Peter Willadt <willadt@t-online.de>
 */

namespace Lespf\LespfCalendar\Domain\Model;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Configuration\Loader\YamlFileLoader;

use Lespf\LespfCalendar\Utility\DateUtility;

/**
 * generate month calendar,
 * include holidays and events (if wanted)
 */
class Calendar extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * holidayArray holds holidays
     * @var array<int, int>
     */
    protected $holidayArray = [];
    /**
     * moonArray holds moon phases
     * @var array
     */
    protected $moonArray = [];
    /**
     * recurringEventArray holds all recurring events of the year
     * @var array<int, array>
     */
    protected $recurringEventArray = [];
    /**
     * eventArray holds all events of the current month
     * @var array<int, string>
     */
    protected $eventArray = [];
    /**
     * daySlots holds all the days and placeholders
     * @var array<int, array>
     */
    protected $daySlots = [];
    /**
     * number of month to show, starting from 1
     * if == 0: actual month
     * @var int
     */
    protected $month = 0;
    /**
     * number of year to show, from 1970 to 2035
     * if == 0: current year
     * @var int
     */
    protected $year = 0;
    /** 
     * number of day the wek starts (Europe = 1, US = 7 ...)
     * @var int
     */
    protected $weekStart = 1;
    /**
     * holds name of file with data for holidays 
     * or name of country/region dataset to read from database
     * @var string
     */
    protected $calendarDataFilename = '';
    /**
     * calendar utility for general calendar calculations
     * @var DateUtility
     */
    protected $dateUtility;
    /**
     * holds pid list for events
     * @var array
     */
    protected $pidList = [];
    /**
     * inserts celestial holidays into the holiday table
     * 
     * @param string $base like e.g. easter or spring
     * @param array $common for common settings like the year
     * @param array $entries
     * @return void
     */
    protected function calculateSpecialDateDependentDays(string $base, array $common, array $entries) : void
    {
	$basedate = $this->dateUtility->getSpecialDate($base, $this->year);
	$eventParts = [ 'year', 'month', 'day' ];
        $determinators = [ 'kind', 'name' ];
        if (empty($common['name'])) {
            $common['name'] = $common['kind'];
        }
	foreach ($entries as $entry) {
	    $result = [];
	    foreach ($eventParts as $part) {
		$result[$part] = $basedate[$part];
	    }
	    foreach ($determinators as $part) {
                $result[$part] = htmlspecialchars($entry[$part] ?? $common[$part]);
            }
	    if (! empty($entry['offset'])) {
		$result['day'] += $entry['offset'];
	    }
	    $corrected = $this->dateUtility->adjustDate($result);
	    if ($corrected['year'] == $basedate['year']) {
                if (! empty($result['kind']) and ($result['kind'] == 'holiday' or $result['kind'] == 'partial' or $result['kind'] == 'memorial')) {
                    foreach ($determinators as $part) {
                        $this->holidayArray[$corrected['month']][$corrected['day']][$part] = $result[$part];
                    }
                }
                if (! empty($result['category'])) {
                    $this->$recurringEventArray[$corrected['month']][$corrected['day']][htmlspecialchars($result['category'])] = 1;
                }
	    }
	}
    }
    /**
     * generates holidays from database
     * 
     * @param int $year
     * @param string $region
     * @return bool
     */
    protected function buildHolidayArrayFromDatabase(int $year, string $region) : bool
    {
	$repository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Lespf\LespfCalendar\Domain\Repository\CalendarRepository');
	$repository->setCountryAndRegion($region);
	$rawdata = $repository->getRawHolidayArray($year);
        $common = ['year' => $year, 'kind' => 'holiday'];
	foreach($rawdata as $base => $entries) {
	    if ($base == 0) {
		$this->holidayArray[0] = $entries;
		continue;
	    } else switch($base) {
                case 'easter':
                case 'spring':
                case 'summer':
                case 'fall':
                case 'winter':
                    $this->calculateSpecialDateDependentDays($base, $common, $entries); 
                    break;
                default:
                    $month = $base;
                    foreach ($entries as $entry)  {
                        $result = $this->dateUtility->calculateWeekday($year, $month, $entry);
                        $result['kind'] = $entry['kind'] ?? $common['kind'];
                        $this->holidayArray[$result['month']][$result['day']]['kind'] = htmlspecialchars($result['kind']);
                        $this->holidayArray[$result['month']][$result['day']]['name'] = htmlspecialchars($result['name']);
                    }
                    // break; 
            }
	}
	return true;
    }
    /**
     * Generates Holiday array from database or from YAML file
     * 
     * @return void
     */
    protected function buildHolidayArray(int $year) : void
    {
	if (! empty($this->holidayArray[0]) and $this->holidayArray[0] == $year)
	    return;
        $this->holidayArray = [];
	if (str_ends_with($this->calendarDataFilename, '.yaml')) {
	    $this->buildHolidayArrayFromYaml($year);
	} else {
	    $this->buildHolidayArrayFromDatabase($year, $this->calendarDataFilename);
	}
    }
    /** 
     * formats event catagories for CSS
     * 
     * @param int $day
     * @return string
     */
    protected function getEventClasses(int $listDay) : string
    {
	$result = '';
	if (! empty($this->recurringEventArray[$this->month][$listDay])) {
	    foreach($this->recurringEventArray[$this->month][$listDay] as $class => $dummy) {
		$result .= ' cal-evt-cat-' . $class; 
	    }
	}
	if (! empty($this->eventArray[$listDay])) {
	    foreach($this->eventArray[$listDay] as $class => $dummy) {
		$result .= ' cal-evt-cat-' . $class; 
	    }
	}
	if (! empty($this->moonArray[$this->month][$listDay])) {
	    foreach($this->moonArray[$this->month][$listDay] as $class => $dummy) {
		$result .= ' cal-' . $class;
	    }
	}
	return $result;
    }
    /**
     * builds the weeks
     * 
     * @return void
     */
    protected function generateWeeks() : void
    {
	$headings = array('MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU');
	$this->daySlots = array();
	$nextDaySlot = 0;
        $firstDayInMonth = new \DateTimeImmutable($this->year . '-' . $this->month . '-01 00:00 UTC');
	$daysInMonth = $this->dateUtility->getMonthLength($this->year, $this->month);;
	$firstWeekday = $firstDayInMonth->format('w');
        $weekNumber = intval($firstDayInMonth->format('W'));
        $weekNumberFormatted = sprintf('%02d', $weekNumber);
        $this->buildHolidayArray($this->year);
	$repository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Lespf\LespfCalendar\Domain\Repository\EventRepository');
	$repository->setPidList($this->pidList);
	$this->eventArray = $repository->getEventCategories($this->year, $this->month, $this->eventArray);
	$this->integrateMoonPhases();
	if ($firstWeekday == 0)	{
	    $firstWeekday = 7;
	}
	if ($firstWeekday != $this->weekStart)	{
            if ($firstWeekday < $this->weekStart % 7)	{
                $firstWeekday += 7;
            }
            // insert blank days for the rest of last month
	    for ($x = ($this->weekStart % 7); $x < $firstWeekday; $x++)	{
		$this->daySlots[$nextDaySlot] = array('number' => '',  'class' => 'cal-day-out-of-range', 'weeknumber' => $weekNumberFormatted);
		$nextDaySlot++;
	    }
	    if ($firstWeekday > 7) {
                $firstWeekday -= 7;
            }
	}
	// is the current day within the displayed month?
	$isTodayInCalendarMonth = 0;
	if (($this->year == $this->today->format('Y')) and ($this->month == $this->today->format('n')))  {
	    $isTodayInCalendarMonth = $this->today->format('j');
	}
	// construct the days
	$runningDay = $firstWeekday - 1;
        if ($runningDay < 0) {
            $runningDay += 7;
        }
	for($listDay = 1; $listDay <= $daysInMonth; $listDay++)	{
            $dayName = '';
	    $dayClass = 'cal-weekday-' . $headings[$runningDay % 7];
	    if (! empty($this->holidayArray[$this->month][$listDay]['kind']))    {
		$dayClass .= ' cal-evt-' . $this->holidayArray[$this->month][$listDay]['kind'];
                if (! empty($this->holidayArray[$this->month][$listDay]['name'])) {
                    $dayName = $this->holidayArray[$this->month][$listDay]['name'];
                }
	    }
	    if ($listDay == $isTodayInCalendarMonth)  {
		$dayClass .= ' cal-today';
	    }
	    $dayClass .= $this->getEventClasses($listDay);
	    $daynumberFormatted = sprintf('%02d', $listDay);
 	    $this->daySlots[$nextDaySlot] = [
                                             'number' => $daynumberFormatted,
                                             'class' => $dayClass,
                                             'name' => $dayName,
                                             'weekday' => $headings[$runningDay % 7],
                                             'weeknumber' => $weekNumberFormatted
                                             ];
            if (! empty($this->holidayArray[$this->month][$listDay]['kind']))    {
                // this (and the weekday) makes it easier to pick the kind of day, if the array is accessed from another class
                $this->daySlots[$nextDaySlot]['kind'] = $this->holidayArray[$this->month][$listDay]['kind'];
            }
            if (($runningDay % 7) == 0) {
                // get next week number.
                $actualDay = new \DateTimeImmutable($this->year . '-' . $this->month . '-' . $listDay . ' 00:00 UTC');
                $weekNumber = intval($actualDay->format('W'));
                $weekNumberFormatted = sprintf('%02d', $weekNumber);
            }
	    $nextDaySlot++;
	    $runningDay++;
	}
	// perhaps some more blank days ...
        $dummyDay = $nextDaySlot % 7;
	if ($dummyDay != 0)	{
	    for($i = 0; $i < (7 - $dummyDay); $i++)	{
		$this->daySlots[$nextDaySlot] = array('number' => '',  'class' => 'cal-day-out-of-range', 'weeknumber' => $weekNumberFormatted);
		$nextDaySlot++;
	    }
	}
	return;
    }
    /**
     * Initialize data and connection pool
     */
    public function __construct()
    {
	$this->today = new \DateTimeImmutable('now');
	$this->year = (int) $this->today->format('Y');
	$this->month = (int) $this->today->format('n');
	$this->dateUtility = GeneralUtility::makeInstance(DateUtility::class);
        // can not use dependency injection for here
	$this->weekStart = intval($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['lespf_calendar']['weekStart'] ?? 1);
    }
    
    /**
     * Returns the month
     *
     * @return int
     */
    public function getMonth() : int
    {
	return $this->month;
    }
    /**
     * gets day number of start of week (Europe = 1 for monday)
     * 
     * @return int
     */
    public function getWeekStart() : int
    {
	return $this->weekStart;
    }
    /**
     * Sets the month
     * 
     * @param int $month
     * @return void
     */
    public function setMonth(int $month) : void
    {
	$month = intval($month);
	if ($month >= 1 and $month <= 12)
	    $this->month = $month;
    }
    /**
     * Returns the year
     * 
     * @return int
     */
    public function getYear() : int
    {
	return $this->year;
    }
    /**
     * Sets the year
     * 
     * @param int $yearFrom
     * @return void
     */
    public function setYear(int $year) : void
    {
	$year = 0 + $year;
	if ($year >= 1970 and $year <= 2037) {
	    $this->year = $year;
        }
	$this->generateWeeks();
    }
    /**
     * Returns next month (for links)
     * 
     * @return array<string, int>
     */
    public function getNextMonth() : array
    {
	$nextmonth = $this->month + 1;
	$nextyear = $this->year;
	if ($nextmonth > 12) {
	    $nextyear ++;
	    $nextmonth -= 12;
	}
	return array('year' => $nextyear, 'month' => $nextmonth);
    }
    /**
     * Returns previous month (for links)
     * 
     * @return array<string, int>
     */
    public function getPrevMonth() : array
    {
	$prevmonth = $this->month - 1;
	$prevyear = $this->year;
	if ($prevmonth < 1) {
	    $prevyear --;
	    $prevmonth = 12;
	}
	return array('year' => $prevyear, 'month' => $prevmonth);
    }
    /**
     * Returns single day
     * 
     * @param int $daynumber
     * @return array
     */
    public function getDay(int $dayNumber): array
    {
        if ($dayNumber < 1 or $dayNumber > 31) {
            return [];
        }
	if (empty($this->daySlots)) {
	    generateWeeks();
        }
        foreach ($this->daySlots as $slot) {
            if ($slot['number'] == $dayNumber)  {
                return $slot;
            }
        }
        return [];
    }
    
    /**
     * Returns single day slot
     * 
     * @param int $row
     * @param int $col
     * @return array
     */
    public function getDaySlot(int $row, int $col): array
    {
	if (empty($this->daySlots)) {
	    generateWeeks();
        }
	return $this->daySlots[$row * 7 + $col] ?? [];
    }
    /**
     * Returns all day slots
     * 
     * @param int, int
     * @return array
     */
    public function getDaySlots(): array
    {
	if (empty($this->daySlots)) {
	    generateWeeks();
        }
	return $this->daySlots;
    }
    /** sets $pidList
     * can't build by myself. It's the Controller's job
     * call this before setting the month.
     * 
     * @param array
     * @return void
     */
    public function setPidList($pidList) : void
    {
	$this->pidList = $pidList;
    }
    /**
     * Sets the calendarDataFilename.
     * 
     * @param string $calendarDataFilename of name of region
     * @return void
     */
    public function setCalendarDataFilename(string $calendarDataFilename) : void
    {
	$this->calendarDataFilename = $calendarDataFilename;
    }
    /**
     * integrate moon phases
     * 
     * @return void
     */
    protected function integrateMoonPhases() : void
    {
	$this->moonArray = [];
	$months = $this->dateUtility->getMoonPhases($this->year);
	foreach ($months as $month => $days) {
	    foreach ($days as $day => $phase) {
		$this->moonArray[$month][$day]['moon-' . $phase] = 1;
	    }
	}
    }
    /*********************************************************************
     * YAML stuff
     ********************************************************************/
    /**
     * Insert any recurring events from yaml into category array
     * 
     * @param int $year
     * @param array $recurring
     * @return void
     */
    protected function buildRecurrentEvents(array $common, array $recurring) : void
    {
	$this->recurringEventArray = [];
	foreach($recurring as $monthrecord) {
	    if (empty($monthrecord['entries'])) {
                continue;
            }
            if(! empty($monthrecord['year']) and  ($monthrecord['year'] != $common['year'])) {
                continue;
	    }
	    if (! empty($monthrecord['month'])) {
                $months = [];
                if (preg_match('/^\d+(\s*,\s*\d+)*$/', strval($monthrecord['month'])) == 1) {
                    $months = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', strval($monthrecord['month'])); 
                } else {
                    $months[] = intval($monthrecord['month']);
                }
                foreach($months as $month) {
                    foreach ($monthrecord['entries'] as $entry)  {
                        $result = $this->dateUtility->calculateWeekday($common['year'], $month, $entry);
                        if ($result['year'] != $common['year'])  {
                            continue;
                        }
                        $duration = $entry['duration'] ?? 1;
                        if ($duration < 1) {
                            $duration = 1;
                        }
                        $category = htmlspecialchars(strval($entry['category']));
                        $date = [ 'year' => $common['year'], 'month' => $result['month'], 'day' => $result['day']];
                        for ($x = 0; $x < $duration; $x++) {
                            $date = $this->dateUtility->adjustDate($date);
                            if ($date['year'] == $common['year']) {
                                $this->recurringEventArray[$date['month']][$date['day']][$category] = 1;
                                $date['day']++;
                            }
                        }
                    }
                }
	    }else {
		if (! empty($monthrecord['base'])) {
		    $this->calculateSpecialDateDependentDays($monthrecord['base'], $common, $monthrecord['entries']);
		}
	    }
	}
    }
    /**
     * Insert holidays (full or partial) from YAML into holiday array
     * can't be done directly, as some reordering of data has to be done to be compatible with database
     * 
     * @param array $common
     * @param array $holidays
     * @return void
     */
    protected function processYamlHolidays(array $common, array $holidays) : void
    {
	$this->recurringEventArray = [];
	foreach($holidays as $monthrecord) {
	    if (empty($monthrecord['entries'])) {
                continue;
            }
	    if (! empty($monthrecord['year']) and  ($monthrecord['year'] != $common['year'])) {
		continue;
	    }
	    if (! empty($monthrecord['month'])) {
                $month = $monthrecord['month'];
                foreach ($monthrecord['entries'] as $entry)  {
                    $resulting = $this->dateUtility->calculateWeekday($common['year'], $month, $entry);
                    $resulting['kind'] = htmlspecialchars($entry['kind'] ?? $common['kind']);
                    $resulting['name'] = htmlspecialchars($entry['name'] ?? 'unnamed day');
                    $this->holidayArray[$resulting['month']][$resulting['day']]['kind'] = $resulting['kind'];
                    $this->holidayArray[$resulting['month']][$resulting['day']]['name'] = $resulting['name'];
		}
		continue;
	    } else {
                if (! empty($monthrecord['base'])) {
                    $this->calculateSpecialDateDependentDays($monthrecord['base'], $common, $monthrecord['entries']);
                }
	    }
	}
    }
    /**
     * generates holidays and events from YAML
     * 
     * @param int $year
     * @return bool
     */
    protected function buildHolidayArrayFromYaml(int $year) : bool
    {
        $sections = [ 'holidays' => 'holiday', 'partials' => 'partial', 'memorials' => 'memorial'];
        $common = [ 'year' => $year];
	if ( empty($this->calendarDataFilename)
	    or (! str_ends_with($this->calendarDataFilename, '.yaml'))
	    or (! file_exists($this->calendarDataFilename))) {
	    return false;
	}
	try {
	    $data = (new YamlFileLoader())->load($this->calendarDataFilename, YamlFileLoader::PROCESS_IMPORTS);
	} catch(Exception $e) {
	    return false;
	}
	if (! empty($data['include'])) {
	    // use country defaults from database
	    $this->buildHolidayArrayFromDatabase($year, $data['include']);
	}
	$this->holidayArray[0] = $year;
        foreach ($sections as $sectionName => $kind) {
            if (! empty($data[$sectionName])) {
                $common['kind'] = $kind;
                $this->processYamlHolidays($common, $data[$sectionName]);
            }
	}
	if (!empty($data['recurring'])) {
            $common['kind'] = 'event';
	    $this->buildRecurrentEvents($common, $data['recurring']);
	}
	return true;
    }
    // Overrides 
    public function _isDirty($propertyName = null) : bool
    {
	return false;
    }
    public function getUid(): ? int
    {
	// can't do it, Typo3 would query the database
	// return $this->year * 100 + $this->month;
	return 0;
    }
}

// \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($variable);
