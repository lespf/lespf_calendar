<?php

    declare(strict_types=1);

/**
 * This file is part of the "LesPf Calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Peter Willadt <willadt@t-online.de>
 */

namespace Lespf\LespfCalendar\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;

use Lespf\LespfCalendar\Utility\DateUtility;

/*
 * This is not a repository for calendars.
 * The calendar is generated on the fly.
 * Here we fetch some information from the database
 * concerning events.
 */

class EventRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * Table to query for events
     * @var string
     */
    protected $eventProvider = 'none';
    /**
     * number of month to show, starting from 1
     * if = 0: actual month
     * @var int
     */
    protected $month = 0;
    /**
     * number of year to show, from 1970 to 2035
     * if = 0: current year
     * @var int
     */
    protected $year = 0;
    /**
     * calendar utility for general calendar calculations
     * @var DateUtility
     */
    protected $dateUtility;
    /** 
     * pidList holds ids of pages containing events to consider
     * @var array<int, int>
     */
    protected $pidList = [];
    /**
     * eventArray holds all events of the current month
     * @var array<int, string>
     */
    protected $eventArray = [];

    public function __construct()
    {
	$this->eventProvider = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['lespf_calendar']['eventSource'];
	$this->dateUtility = GeneralUtility::makeInstance(DateUtility::class);
    }
    /**
     * get table and column names
     * 
     * @return array <string,string>
     */
    protected function getEventProviderData()
    {
	if ($this->eventProvider == 'eventnews') {
	    $eventProviderData = [ 'table' => 'tx_news_domain_model_news',
				   'startcol' => 'datetime',
				   'endcol' => 'event_end',
				   'catcol' => 'categories',
				   'pidcol' => 'pid',
				   'uidcol' => 'uid'];
	} elseif ($this->eventProvider == 'news') {
	    $eventProviderData = [ 'table' => 'tx_news_domain_model_news',
				   'startcol' => 'datetime',
				   'endcol' => 'datetime',
				   'catcol' => 'categories',
				   'pidcol' => 'pid',
				   'uidcol' => 'uid'];
	} elseif ($this->eventProvider == 'sfeventmgt') {
	    $eventProviderData = [ 'table' => 'tx_sfeventmgt_domain_model_event',
				   'startcol' => 'startdate',
				   'endcol' => 'enddate',
				   'catcol' => 'category',
				   'pidcol' => 'pid',
				   'uidcol' => 'uid'];
	} else {
	    return null;
	}
	return $eventProviderData;
    }
    /** get Duration of an event, trimmed to actual month
     * 
     * @param int $startdate first day of event
     * @param int $enddate   last day of event
     * @return array<string, int> | null
     */
    protected function getEventDuration(int $startdate, int $enddate) : ?array
    {
	$duration = [];
	if ($enddate < $startdate)
	    return null;
	$eventStartDate = new \DateTime();
	$eventStartDate->setTimestamp($startdate);
	if ((int)$eventStartDate->format('Y') < $this->year or (int)$eventStartDate->format('n') < $this->month) {
	    $duration['first'] = 1;
	} elseif ((int)$eventStartDate->format('n') > $this->month) {
	    return null;
	} else {
	    $duration['first'] = $eventStartDate->format('j');
	}
	$eventEndDate = new \DateTime();
	$eventEndDate->setTimestamp($enddate);
	if ( ((int)$eventEndDate->format('Y') > $this->year) or ((int)$eventEndDate->format('n') > $this->month)) {
	    $duration['last'] = (int)$eventStartDate->format('t');
	} else {
	    $duration['last'] = (int)$eventEndDate->format('j');
	}
	$duration['days'] = $duration['last'] - $duration['first'] + 1;
	return $duration;
    }
    /**
     * get List of uids that are connected with exclusion categories
     * 
     * @param array $categoryList
     * @return string
     */
    protected function getExclusionDql(array $categoryList) : string
    {
	$catQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_category_record_mm');
	return $catQueryBuilder->select('uid_foreign')
	    ->from('sys_category_record_mm')
	    ->where($catQueryBuilder->expr()->in('uid_local',
						 $catQueryBuilder->createNamedParameter($categoryList, Connection::PARAM_INT_ARRAY)))
	    ->getSQL();
    }
    /**
     * fetch event categories and build array for CSS
     * 
     * @param array $events
     */
    protected function generateEventCategories(array $events) : void
    {
	// categories to watch or to exclude
	$categoryList = [];
	// include only them, exclude them, or use all categories
	$treatCategoryList = 'ignore'; // or 'exclude' or 'include'
	$keywords = ['include', 'exclude', 'exclude (strict)'];
	$tempTreatment = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['lespf_calendar']['categoryListTreatment'] ?? 'ignore';
	$tempList = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['lespf_calendar']['categoryList'];
	if (! empty($tempList)) {
	    foreach($keywords as $keyword) {
		if (($tempTreatment == $keyword)  and preg_match('/^\d+(\s*,\s*\d+)*$/', $tempList)) {
		    $categoryList = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $tempList);
		    $treatCategoryList = $tempTreatment;
		    break;
		}
	    }
	}
	$eventProviderData = $this->getEventProviderData();
	// prepare with multiple execute is apparently not possible any more
	$catQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_category_record_mm');
	foreach($events as $event){
	    if (($eventDuration = $this->getEventDuration($event[$eventProviderData['startcol']], $event[$eventProviderData['endcol']])) == null)
		continue;
	    if ($event[$eventProviderData['catcol']] == 0) {
		if ($treatCategoryList != 'include') {
		    // events without categories get mentioned
		    // if the list of allowed categories is not restricted
		    for ($i = 0; $i < $eventDuration['days']; $i++){
			if (empty($this->eventArray[$i + $eventDuration['first']])){
			    $this->eventArray[$i + $eventDuration['first']] = [];
			}
			$this->eventArray[$i + $eventDuration['first']]['evt'] = 1;
		    }
		}
		continue;
	    }
	    switch ($treatCategoryList) {
		case 'include':
		$wherecondition = $catQueryBuilder->expr()->and(
								$catQueryBuilder->expr()->eq('uid_foreign', $event[$eventProviderData['uidcol']]),
								$catQueryBuilder->expr()->eq('tablenames', $catQueryBuilder->createNamedParameter($eventProviderData['table'])),
								$catQueryBuilder->expr()->in('uid_local', 
											     $catQueryBuilder->createNamedParameter($categoryList, Connection::PARAM_INT_ARRAY)));
		break;
		case 'exclude':
		$wherecondition = $catQueryBuilder->expr()->and($catQueryBuilder->expr()->eq('uid_foreign', $event[$eventProviderData['uidcol']]),
								$catQueryBuilder->expr()->eq('tablenames', 
											     $catQueryBuilder->createNamedParameter($eventProviderData['table'])),
								$catQueryBuilder->expr()->notIn('uid_local', $catQueryBuilder->createNamedParameter($categoryList, Connection::PARAM_INT_ARRAY)));
		break;
		case 'exclude (strict)':
		$exclusionDql = $this->getExclusionDql($categoryList);
		$wherecondition = $catQueryBuilder->expr()->and($catQueryBuilder->expr()->eq('uid_foreign', $event[$eventProviderData['uidcol']]),
								$catQueryBuilder->expr()->eq('tablenames', 
											     $catQueryBuilder->createNamedParameter($eventProviderData['table'])),
								$catQueryBuilder->expr()->notIn('uid_local', $catQueryBuilder->createNamedParameter($categoryList, Connection::PARAM_INT_ARRAY)),
								$catQueryBuilder->expr()->notIn('uid_foreign', $exclusionDql));
		break;
		default:
		$wherecondition = $catQueryBuilder->expr()->and($catQueryBuilder->expr()->eq('uid_foreign', $event[$eventProviderData['uidcol']]),
								$catQueryBuilder->expr()->eq('tablenames', 
											     $catQueryBuilder->createNamedParameter($eventProviderData['table'])));
		// break;
	    }
	    $categories = $catQueryBuilder->select('uid_local')->from('sys_category_record_mm')
		->where($wherecondition)->executeQuery()->fetchAllAssociative();
	    for ($i = 0; $i < $eventDuration['days']; $i++){
		if (empty($this->eventArray[$i + $eventDuration['first']])){
		    $this->eventArray[$i + $eventDuration['first']] = [];
		}
		foreach($categories as $category){
		    $this->eventArray[$i + $eventDuration['first']][$category['uid_local']] = 1;
		}
	    }
	}
	return;
    }
    /**
     * get timestamp of beginning of month
     * 
     * @param int $year
     * @param int $month
     * @return int
     */
    protected function getStartTimestamp(int $year, int $month) : int
    {
	$firstDayInMonth = new \DateTimeImmutable($year . '-' . $month . '-01 00:00 UTC');
	return $firstDayInMonth->getTimestamp();
    }
    /**
     * get timestamp of end of month
     * 
     * @param int $year
     * @param int $month
     * @return int
     */
    protected function getEndTimestamp(int $year, int $month) : int
    {
	$newMonth = [ 'year' => $year, 'month' => $month + 1, 'day' => 1 ];
	$newMonth = $this->dateUtility->adjustDate($newMonth);
	$firstDayNextMonth = new \DateTimeImmutable($newMonth['year'] . '-' . $newMonth['month'] . '-01 00:00 UTC');
	return $firstDayNextMonth->getTimestamp();
    }
    /**
     * query event informations from database
     * 
     * @param int $year
     * @param int $month
     * @return array
     */
    protected function generateEvents(int $year, int $month)
    {
	if (($eventProviderData = $this->getEventProviderData()) == null) {
	    return [];
	}
	$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($eventProviderData['table']);
	$startdate = $this->getStartTimestamp($year, $month);
	$enddate = $this->getEndTimestamp($year, $month);
	if(empty($this->pidList)) {
	    $events = $queryBuilder
		->select($eventProviderData['uidcol'], $eventProviderData['startcol'], $eventProviderData['endcol'], $eventProviderData['catcol'])
		->from($eventProviderData['table'])
		->where($queryBuilder->expr()->and(
						   $queryBuilder->expr()->gte($eventProviderData['endcol'], $startdate),
						   $queryBuilder->expr()->lt($eventProviderData['startcol'], $enddate))
			)
		->executeQuery()->fetchAllAssociative();
	} else {
	    $events = $queryBuilder
		->select($eventProviderData['uidcol'], $eventProviderData['startcol'], $eventProviderData['endcol'], $eventProviderData['catcol'])
		->from($eventProviderData['table'])
		->where($queryBuilder->expr()->and(
						   $queryBuilder->expr()->gte($eventProviderData['endcol'], $startdate),
						   $queryBuilder->expr()->lt($eventProviderData['startcol'], $enddate),
						   $queryBuilder->expr()->in($eventProviderData['pidcol'], 
									     $queryBuilder->createNamedParameter($this->pidList, Connection::PARAM_INT_ARRAY))))
		->executeQuery()->fetchAllAssociative();
	}
	$this->generateEventCategories($events);
    }
    /**
     * retrieve event categories for the days of the month given
     * 
     * @param int $month
     * @param int $year
     * @return array
     */
    public function getEventCategories(int $year, int $month, array $eventArray) {
	$this->year = $year;
	$this->month = $month;
	$this->eventArray = $eventArray;
	$this->generateEvents($year, $month);
	return $this->eventArray;
    }
    /**
     * set list of pages with events to be considered
     * 
     * @param int $pids
     *
     */
    public function setPidList(array $pids) : void
    {
	$this->pidList = $pids;
    }
}
