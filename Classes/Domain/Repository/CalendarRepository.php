<?php

declare(strict_types=1);

/**
 * This file is part of the "LesPf Calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Peter Willadt <willadt@t-online.de>
 */

namespace Lespf\LespfCalendar\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;

/*
 * In the database, there are only informations to
 * generate a calendar on the fly, so this is not 
 * a real repository.
 *
 * This is not best style, but Extbase provides 
 * some resources only to some classes.
 */

class CalendarRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
  /**
   * Country for holidays
   * @var string
   */
  protected $country = '';
  /**
   * region for holidays
   * @var string
   */
  protected $region = '';
  /**
   * rawHolidayArray holds all base data to calculate holidays
   * @var array
   */
  protected $rawHolidayArray = [];
  /** 
   * set country and region from a string like DE-BW
   * 
   * @param $regionstring
   * @return void
   */
  public function setCountryAndRegion (string $regionstring) : void
  {
      $regionstring = strtoupper($regionstring);
      $regionstring = trim($regionstring);
      $result = preg_match('/^(\w\w)-(\w+)$/', $regionstring, $matches);
      if ($result > 0) {
	  $temp = explode('-', $regionstring);
	  $this->country = $temp[0];
	  $this->region = $temp[1];
	  return;
      } elseif (preg_match('/^[A-Z]{2}$/', $regionstring, $matches)) {
	  $this->country = $regionstring;
	  $this->region = '';
      }
  }
  /**
   * query the database for holidays, if possible
   * 
   * @param int $year
   * @return bool
   */
  protected function buildRawHolidayArray(int $year) : bool
  {
      $tablename = 'tx_lespfcalendar_domain_model_calendar';
      $yearList = [ 0 ];
      $eventTypes = ['holiday', 'partial', 'memorial'];
      $plainfields = ['weekday', 'shifting', 'holidayname', 'kind'];
      $specialfields = ['base', 'monthday', 'distance', 'holidayname'];
      $allfields = array_merge($plainfields, $specialfields);
      if (empty($this->country)) {
	  return false;
      }
      if ($year >= 1970) {
	  $yearList = [ 0, $year];
      }
      $this->rawHolidayArray = [];
      $this->rawHolidayArray[0] = $year;
      $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($tablename);
      if (! empty($this->region)) {
	  $regionArray = ['', $this->region];
	  $wherecondition = $queryBuilder->expr()->and(
						       $queryBuilder->expr()->eq('country', $queryBuilder->createNamedParameter($this->country)),
						       $queryBuilder->expr()->in('kind',
										 $queryBuilder->createNamedParameter($eventTypes, Connection::PARAM_STR_ARRAY)),
						       $queryBuilder->expr()->in('specialyear',
										 $queryBuilder->createNamedParameter($yearList, Connection::PARAM_INT_ARRAY)),
						       $queryBuilder->expr()->in('region',
										 $queryBuilder->createNamedParameter($regionArray, Connection::PARAM_STR_ARRAY))
					     )
	      ;
      } else {
	  $wherecondition = $queryBuilder->expr()->and(
					     $queryBuilder->expr()->eq('country',  $queryBuilder->createNamedParameter($this->country)),
					     $queryBuilder->expr()->in('kind',
								       $queryBuilder->createNamedParameter($eventTypes, Connection::PARAM_STR_ARRAY)),
					     $queryBuilder->expr()->in('specialyear',
								       $queryBuilder->createNamedParameter($yearList, Connection::PARAM_INT_ARRAY)),
					     $queryBuilder->expr()->eq('region', $queryBuilder->createNamedParameter(''))
					     )
	      ;
      }
      $rawdata = $queryBuilder->select(...$allfields)
          ->from($tablename)
	  ->where($wherecondition)
	  ->executeQuery()->fetchAllAssociative();
      foreach ($rawdata as $record) {
	  if (empty($record['base'])) {
	      continue;
	  }
	  if(empty($this->rawHolidayArray[$record['base']])) {
	      $this->rawHolidayArray[$record['base']] = [];
	  }
	  $detail = [];
	  if (! empty($record['monthday'])) {
	      $detail['day'] = $record['monthday'];
	  }
	  if (! empty($record['distance'])) {
	      $detail['offset'] = $record['distance'];
	  }
	  if (! empty($record['holidayname'])) {
	      $detail['name'] = htmlspecialchars($record['holidayname']);
	  }
	  foreach ($plainfields as $field) {
	      if (! empty($record[$field])) {
		  $detail[$field] = $record[$field];
	      }
	  }
	  $this->rawHolidayArray[$record['base']][] = $detail;
      }
      return true;
  }
  /**
   * get array with raw Holiday Data
   * the year is only relevant for irregular holidays
   * (e.g. they happen only on some years)
   * 
   * @param int $year
   * @return array
   */
  public function getRawHolidayArray(int $year) : array
  {
      if ($this->buildRawHolidayArray($year) == false) {
	  return [];
      }
      return $this->rawHolidayArray;
  }
}
