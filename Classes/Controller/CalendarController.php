<?php

    declare(strict_types=1);

/**
 * This file is part of the "LesPf Calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Peter Willadt <willadt@t-online.de>
 */

namespace Lespf\LespfCalendar\Controller;
use TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Lespf\LespfCalendar\Utility\QueryGenerator;

/**
 * CalendarController
 */
class CalendarController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /** 
     * holds pids for events
     * @var array
     */
    protected $pidList = [];
    /** @var QueryGenerator */
    protected $queryGenerator;

    public function __construct()
    {
	$this->queryGenerator = GeneralUtility::makeInstance(QueryGenerator::class);
    }
    
    /**
     * get path to yaml file with region data or of region
     * 
     * @return string
     */ 
    protected function getCalendarDataFilename() : string
    {
	$region = $this->settings['region'] ?? 'DE-BW';
	if (! str_ends_with($region, '.yaml')) {
	    // fetch data from database
	    return basename($region);
	}
	$region = basename($region);
	$specialpath = $this->settings['customeventpath'] ?? ''; // e.g. 'fileadmin/'
	if (! empty($specialpath)) {
	    if (! str_ends_with($specialpath, '/') and ! str_ends_with($specialpath, '\\')) {
		$specialpath .= DIRECTORY_SEPARATOR;
	    }
	    $myfile = GeneralUtility::getFileAbsFilename($specialpath . $region);
	    if (file_exists($myfile)) {
		return $myfile;
	    }
	}
	$defaultpath = 'EXT:lespf_calendar/Resources/Private/Regions/';
	$myfile = GeneralUtility::getFileAbsFilename($defaultpath . $region);
	if (file_exists($myfile)) {
	    return $myfile;
	}
	return '';
    }
    /**
     * action list
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listAction() : \Psr\Http\Message\ResponseInterface
    {
	$mycal = new \Lespf\LespfCalendar\Domain\Model\Calendar();
	$today = new \DateTime('now');
	$startmonth = (int) ($this->settings['month'] ?? $mycal->getMonth());
	$startyear = (int) ($this->settings['year'] ?? $mycal->getYear());
	if (($startmonth < 1) or ($startmonth > 12)) {
	    $startmonth = (int) $today->format('n');
	}
	if (($startyear < 1970) or ($startyear > 2037)) {
	    $startyear = (int) $today->format('Y');
	}
	$calendars = [];
	$this->buildPidList();
	$cachedFilename = $this->getCalendarDataFilename();
	for ($i = 0; $i < 4; $i++) {
	    $calendars[$i] = new \Lespf\LespfCalendar\Domain\Model\Calendar();
	    $calendars[$i]->setPidList($this->pidList);
	    $calendars[$i]->setCalendarDataFilename($cachedFilename);
	    $calendars[$i]->setMonth($i + $startmonth);
	    $calendars[$i]->setYear(0 + $startyear);
	    $startmonth++;
	    if ($startmonth > 12){
		$startmonth = 1;
		$startyear++;
	    }
	}
	$this->view->assign('calendars', $calendars);
	return $this->htmlResponse();
    }
    /**
     * action show
     *
     * // @param month
     * // @param year
     * @return \Psr\Http\Message\ResponseInterface
     */
    //public function showAction(\Lespf\LespfCalendar\Domain\Model\Calendar $calendar): \Psr\Http\Message\ResponseInterface
    public function showAction() : \Psr\Http\Message\ResponseInterface
    {
	$this->buildPidList();
	$cal = new \Lespf\LespfCalendar\Domain\Model\Calendar();
	$cal->setPidList($this->pidList);
	$cal->setCalendarDataFilename($this->getCalendarDataFilename());
	$cal->setMonth(0 + $this->request->getArgument('month'));
	$cal->setYear(0 + $this->request->getArgument('year'));
	$this->view->assign('calendar', $cal);
	return $this->htmlResponse();
    }
     /**
     * Retrieves subpages of given pageIds recursively until reached $this->settings['recursive']
     * Code copied from tt_address 
     *
     * @return bool
     */
    protected function buildPidList() : bool
    {
	if(empty($this->settings['pages'])) {
	    return false;
	}
        $rootPIDs = explode(',', $this->settings['pages']);
        $pidList = $rootPIDs;
        // iterate through root-page ids and merge to array
        foreach ($rootPIDs as $pid) {
            $result = $this->queryGenerator->getTreeList($pid, (int)($this->settings['recursive'] ?? 0));
            if ($result) {
                $subtreePids = explode(',', $result);
                $pidList = array_merge($pidList, $subtreePids);
            }
        }
	$this->pidList = $pidList;
        return (! empty($this->pidList));
    }   
}
