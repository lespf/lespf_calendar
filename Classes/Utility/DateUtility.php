<?php

    declare(strict_types=1);

/**
 * This file is part of the "LesPf Calendar" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Peter Willadt <willadt@t-online.de>
 */

namespace Lespf\LespfCalendar\Utility;

    
class DateUtility
{
    /**
     * get monthlength; only valid from 1901 to 2099
     * 
     * @param int $year important because of leap years
     * @param int $month
     * @return int month length in days
     */
    public function getMonthLength(int $year, int $month) : int
    {
	$monthLengths = [ 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
	// we only do dates from 1970 to 2038, so:
	if (($year % 4) == 0)  {
	    $monthLengths[2] = 29;
	}
	return $monthLengths[$month];
    }
    /** 
     * correct date, if day outside of month given
     * 
     * @param array $date with fields year, month, and day
     * @return array $corrected
     */
    public function adjustDate(array $date) : array
    {
	$fields = ['day', 'month', 'year'];
	foreach ($fields as $field) {
	    $date[$field] = intval($date[$field]);
	}
	if ($date['month'] >= 1 and $date['month'] <= 12 and 
	    $date['day'] > 0 and $date['day'] <= $this->getMonthLength(intval($date['year']), $date['month'])) {
	    return $date;
	}
	while($date['month'] > 12) {
	    $date['year']++;
	    $date['month'] -= 12;
	}
	while($date['month'] < 1) {
	    $date['year']--;
	    $date['month'] += 12;
	}
	while ($date['day'] < 1) {
	    if ($date['month'] > 1) {
		$date['month']--;
	    }else {
		$date['month'] = 12;
		$date['year']--;
	    }
	    $date['day'] += $this->getMonthLength($date['year'], $date['month']);
	}
	while ($date['day'] > $this->getMonthLength($date['year'], $date['month'])) {
	    $date['day'] -= $this->getMonthLength($date['year'], $date['month']);
	    if ($date['month'] > 11) {
		$date['month'] = 1;
		$date['year']++;
	    }else {
		$date['month']++;
	    }
	}
	return $date;
    }
    /**
     * get a weekday number, or -1 if no day given.
     * 
     * @param string $weekdayname english two letters upper case
     * @return int from 1 for monday to 7 for sunday
     */
    protected function getWeekdayNumber(?string $weekdayname) : int
    {
	$weekdays = [ 'MO' => 1, 'TU' => 2, 'WE' => 3, 'TH' => 4, 'FR' => 5, 'SA' =>6, 'SU' => 7 ];
	if (! empty($weekdayname) and ! empty($weekdays[strtoupper($weekdayname)])) {
	    $wantedweekday = $weekdays[strtoupper($weekdayname)];
	    return $wantedweekday;
	}
	return -1;
    }
    /**
     * check if a string forms a valid shifting direction
     * 
     * @param string $correctionString 
     * @return int
     */
    protected function checkCorrectionString(string $correctionString) : int
    {
	if (empty($correctionString)) {
	    return 0;
	}
	if (preg_match('/^\s*(\d+)\s*:\s*(-?\d+)/', $correctionString)) {
	    return 2;
	} elseif (preg_match('/^\s*(\w\w)\s*:\s*(-?\d+)/', $correctionString)) {
	    return 1;
	} else {
	    return -1;
	}
	
    }
    /**
     * get weekday correction in days for holidays that are shifted on e.g. sunday.
     * This is only needed for holidays with an offset and no fixed weekday
     * 
     * @param string $correctionString something like 'SA: 2, SU:1'
     * @param int $weekdayNumber
     * @return int count of days to shift
     */
    protected function getWeekdayCorrection(string $correctionString, int $weekdayNumber) : int
    {
	if (empty($correctionString) or ($this->checkCorrectionString($correctionString) != 1)) {
	    return 0;
	}
	if($weekdayNumber == 0) {
	    $weekdayNumber = 7; 
	}
	$tempArray = explode(',', $correctionString);
	foreach($tempArray as $record) {
	    $matches = [];
	    if (preg_match('/^\s*(\w\w)\s*:\s*(-?\d+)\s*$/', $record, $matches) == true) {
		$correctionWeekdayNumber = $this->getWeekdayNumber($matches[1]);
		if ($correctionWeekdayNumber == $weekdayNumber) {
		    $correction = intval($matches[2]);
		    return $correction;
		}
	    }
	}
	return 0;
    }
    /**
     * get monthday correction in days for events that are to be shifted on some date.
     * This is only needed for events with an offset and a fixed weekday.
     * 
     * @param string $correctionString something like 'SA: 2, SU:1'
     * @param int $weekdayNumber
     * @return int count of days to shift
     */
    protected function getMonthdayCorrection(string $correctionString, int $monthDay) : int
    {
	if (empty($correctionString) or ($this->checkCorrectionString($correctionString) != 2)) {
	    return 0;
	}
	$tempArray = explode(',', $correctionString);
	foreach($tempArray as $record) {
	    $matches = [];
	    if (preg_match('/^\s*(\d+)\s*:\s*(-?\d+)\s*$/', $record, $matches) == true) {
		$correctionMonthday = intval($matches[1]);
		if ($correctionMonthday == $monthDay) {
		    $correction = intval($matches[2]);
		    return $correction;
		}
	    }
	}
	return 0;
    }
    /**
     * calculate the day from an offset and the weekday wanted and massage name security
     * 
     * @param int $year
     * @param int $month
     * @param array $event
     * @return array
     */
    public function calculateWeekday(int $year, int $month, array $event) : array
    {
	$result = [ 'year' => $year, 'month' => $month, 'day' => 0 , 'kind' => $event['kind'] ?? 'holiday'];
	if (! empty($event['name'])) {
	    $result['name'] = htmlspecialchars($event['name']);
	}
	if (! empty($event['day'])) {
	    $result['day'] = $event['day'];
	    return $result;
	}else {
	    // misconfigured. Cant' do anything.
	    if (empty($event['offset']) or ($event['offset'] == 0)) {
		return $result;
	    }
	    $offset = $event['offset'];
	}
	$firstDayInMonth = new \DateTimeImmutable($year . '-' . $month . '-01 00:00 UTC');
	$daysInMonth = $this->getMonthLength($year, $month);
	$firstWeekday = $firstDayInMonth->format('N');
	$wantedweekday = -1;
	if (! empty($event['weekday'])) {
	    $wantedweekday = $this->getWeekdayNumber($event['weekday']);
	}
	if ($wantedweekday <= 0) {
	    // no need to adjust offsets
	    if ($offset  > 0) {
		$result['day'] = $offset;
	    } elseif ($offset  < 0) {
		$result['day'] = $daysInMonth + 1 + $offset;
	    }
	    // what about weekend correction?
	    $weekday = ($result['day'] + $firstWeekday - 1) % 7;
	    if (! empty($event['shifting'])) {
		$shiftday = $this->getWeekdayCorrection($event['shifting'], $weekday);
		if ($shiftday != 0) {
		    $result['day'] += $shiftday;
		}
	    }
	}else {
	    // correct the day
	    $minOffset = $wantedweekday;
	    if ($offset > 0) {
		$day = $offset;
		$weekday = ($day + $firstWeekday - 1) % 7;
		if ($weekday != $minOffset) {
		    $day = $day + (7 + $minOffset - $weekday) % 7;
		}
		$result['day'] = $day;
	    } else {
		$day = $daysInMonth + 1 + $offset;
		$weekday = ($day + $firstWeekday - 1) % 7;
		if($weekday != $minOffset) {
		    $day = $day - (7 + $weekday - $minOffset) % 7;
		}
		$result['day'] = $day;
	    }
	    $corrected = $this->adjustDate($result);
	    if (($corrected['year'] == $result['year']) and ($corrected['month'] == $result['month'])) {
		if (! empty($event['shifting'])) {
		    $shiftday = $this->getMonthdayCorrection($event['shifting'], $result['day']);
		    if ($shiftday != 0) {
			$result['day'] += $shiftday;
		    }
		}
	    }

	}
	$corrected = $this->adjustDate($result);
	if ($corrected['year'] == $result['year']) {
	    foreach ($corrected  as $index => $value) {
		$result[$index] = $value;
	    }
	} else {
	    $result['day'] = 0;
	    $result['month'] = 13;
	}
	return $result;
    }
    /**
     * for things like moon phases or equinoxes, we need to be picky.
     * Typo3 always saves UTC to the database.
     * Timezone offset depends on the date given (because of DST etc.)
     * 
     * @param int $year
     * @param int $month
     * @param int $day
     * @return int $seconds
     */
    protected function getTimeZoneOffset(int $year, int $month, int $day) : int
    {
	$provisionalDate = \DateTimeImmutable::createFromFormat('Y-m-d', $year . '-' . $month . '-' . $day);
	$actualZoneName = date_default_timezone_get();
	$actualZone = new \DateTimeZone($actualZoneName);
	$seconds = $actualZone->getOffset($provisionalDate);
	return $seconds;
    }
    /**
     * get astronomic defined date anchor, e.g. easter, solstices, or equinoxes
     * 
     * @param string $name
     * @param int $year
     * @return array
     */
    public function getSpecialDate(string $name, int $year) : array
    {
	$result = [];
	switch ($name) {
	case 'easter':
	    $easterSaturday = new \DateTime();
	    $easterSaturday->setTimestamp(easter_date($year));
	    $result['year'] = $year;
	    $result['month'] = $easterSaturday->format('n');
	    $result['day'] =$easterSaturday->format('j');
	    break;
	case 'spring':
	case 'summer':
	case 'fall':
	case 'winter':
	default:
	    $result = $this->dayFromJulian($this->getSeasonJulian($name, $year));
	    // break;
	}
	return $result;
    }

    /*
     * Calendar calculations are due to 
     * Jean Meeus: Astronomical Formulae for Calculatores.
     * 4th ed. ISBN 0-943396-22-0
     * 
     * Please excuse the variable names. I have kept them close to Meeus' book 
     * so that it is easier to compare with his work.
     */
    /** 
     * get equinox or solstice 
     * 
     * @param string $season
     * @param int $year
     * @return float Julian date
     */
    protected function getSeasonJulian(string $season, int $year) : float
    {
	$y2 = $year * $year / 1000000.0;
	switch ($season) {
	case 'spring':
	    $result = 1721139.2855 + 365.2421376 * $year +0.0679190 * $y2
		- 0.0027879 * $y2 * $year / 1000.0;
	    break;
	case 'summer':
	    $result = 1721233.2486 + 365.2417284 * $year -0.0530180 * $y2
		+ 0.0093320 * $y2 * $year / 1000.0;
	    break;
	case 'fall':
	    $result = 1721325.6978 + 365.2425055 * $year -0.1266890 * $y2
		+ 0.0019401 * $y2 * $year / 1000.0;
	    break;
	case 'winter':
	    $result = 1721414.3920 + 365.2428898 * $year -0.0109650 * $y2
		- 0.0084885 * $y2 * $year / 1000.0;
	    break;
	default:
	    $result = 0.0;
	    // break;
	}
    
	return $result;
    }
    /** 
     * convert a Julian date to the gregorian calendar
     * also does timezone correction
     * 
     * @param float $julian
     * @return array
     */
    protected function dayFromJulian(float $julian) : array
    {
	$julian += .5;
	$Z = floor($julian);
	$F = $julian - $Z;
	if($Z < 2299161.0)  {
	    $A = $Z;
	}else {
	    $alpha = floor(( $Z - 1867216.25) / 36524.25);
	    $A = $Z + 1 + $alpha - floor($alpha / 4);
	}
	$B = $A + 1524;
	$C = floor(($B - 122.1) / 365.25);
	$D = floor(365.25 * $C);
	$E = floor(($B - $D) / 30.6001);
	$day = intval(floor($B - $D - floor(30.6001 * $E) + $F));
	$month = floor($E) - 1.0;
	if($month > 12.0) {
	    $month -= 12.0;
	}
	$year = intval($C - 4716.0);
	if($month <= 2.5) {
	    $year++;
	}
	$month = intval($month);
	$F += $this->getTimeZoneOffset($year, $month, $day) / 24.0 / 3600.0;
	if ($F > 1.0) {
	    $day++;
	    $F -= 1.0;
	}elseif ($F < 0.0) {
	    $day--;
	    $F += 1.0;
	}
	$result = [ 'year' => $year, 'month' => $month, 'day' => $day ];
	$result = $this->adjustDate($result);
	return $result;
    }
    /**
     * get start value for moon phase computation
     * halfMonth has to go from 0 to 24 for a full year
     * we have to start our loop a little earlier than Jan 1st.
     * 
     * @param int $year
     * @param int $halfmonth
     * @return float
     */
    protected function getMoonPhaseStartNumber(int $year, int $halfMonth) : float
    {
	$k = floor (($year - 1900.0 + ($halfMonth) / 24.0) * 12.3685);
	return $k;
    }
    /**
     * get the next moon phase - starting from 0 for new moon
     * to really start from the beginning, we have to decrease the month in advance
     * this might lead to spurios entries from another time period,
     * that you have to filter afterwards.
     * So please use this function within a loop.
     * Sorry about this.
     * 
     * @param int $year
     * @param int $month
     * @param int $phase
     * @return int
     * */
    protected function getNextMoonPhase(float $k, int $phase) 
    {
	$k += $phase / 4.0;
	$t = $k / 1236.85;
	$julianDate = 2415020.75933 + 29.53058868 * $k + 0.0001178 * $t * $t - 0.000000155  * $t * $t * $t
	    + 0.00033 * sin((166.56 + 132.87 * $t - 0.009173 * $t * $t) * pi() / 180.0);
	$ms = (359.2242 + 29.10535608 * $k - 0.0000333 * $t * $t - 0.00001236 * $t * $t * $t) * pi() / 180.0;
	$mm = (306.0253 + 385.81691806 * $k + 0.0107306 * $t * $t + 0.00001236 * $t * $t * $t) * pi() / 180.0;
	$ml = (21.2964 + 390.67050656 * $k - 0.0016528 * $t * $t - 0.00000239 * $t * $t * $t) * pi() / 180.0;
	$halfCorrection = 0.0028 - 0.0004 * cos($ms) + 0.0003 * cos($mm);
	switch ($phase) {
	case 0: case 2:
	    $julianDate += (0.1734 - 0.000393 * $t) * sin ($ms) + 0.0021 * sin(2 * $ms) - 0.4068 * sin($mm) + 0.0161 * sin(2 * $mm)
		- 0.0004 * sin(3 * $mm) + 0.0104 * sin(2 * $ml) - 0.0051 * sin($ms + $mm) - 0.0074 * sin($ms - $mm)
		+ 0.0004 * sin(2 * $ml + $ms) - 0.0004 * sin(2 * $ml - $ms) - 0.0006 * sin(2 * $ml + $mm) 
		+ 0.0010 * sin(2 * $ml - $mm) + 0.0005 * sin($ms + 2 * $mm);
	    break;
	case 1: 
	case 3:
	    $julianDate += (0.1721 - 0.0004 * $t) * sin($ms) + 0.002 * sin(2 * $ms) - 0.6280 * sin($mm) + 0.0089 * sin(2 * $mm)
		- 0.0004 * sin(3 * $mm) + 0.0079 * sin(2 * $ml) - 0.0119 * sin($ms + $mm) - 0.0047 * sin($ms - $mm)
		+ 0.0003 * sin(2 * $ml + $ms) - 0.0004 * sin(2 * $ml - $ms) - 0.0006 * sin(2 * $ml + $mm)
		+ 0.0021 * sin(2 * $ml - $mm) + 0.0003 * sin($ms + 2 * $mm) + 0.0004 * sin($ms - 2 * $mm)
		- 0.0003 * sin(2 * $ms + $mm);
	    if ($phase == 1) {
		$julianDate += $halfCorrection;
	    } else {
		$julianDate -= $halfCorrection;
	    }
	    break;
	default:
	    $julianDate = 0.0;
	    // break;
	}
	return $julianDate;
    }
    /**
     * get all the Moon Phases for a given Year
     * 
     * @param int $year
     * @return array
     */
    public function getMoonPhases(int $year) : array
    {
	$calendar = [];
	$phases = ['new', 'half-inc', 'full', 'half-dec'];
	for ($halfMonth = 0; $halfMonth < 26; $halfMonth++) {
	    $k = $this->getMoonPhaseStartNumber($year, $halfMonth);
	    for ($phase = 0; $phase < 4; $phase++) {
		$result = $this->dayFromJulian($this->getNextMoonPhase($k, $phase));
		if ($result['year'] != $year) {
		    continue;
		}
		$calendar[$result['month']][$result['day']] = $phases[$phase];
	    }
	}
	return $calendar;
    }
}

