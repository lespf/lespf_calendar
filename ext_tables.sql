create table tx_lespfcalendar_domain_model_calendar (
       country char(2) not null default '',
       region varchar(10) not null default '',
       specialyear integer not null default 0,
       base char(10) not null default '',
       monthday integer,
       distance integer,
       weekday char(2),
       shifting varchar(60),
       kind varchar(20),
       holidayname varchar(80)
);

