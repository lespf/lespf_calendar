.. include:: ../Includes.txt

.. _changelog:

==========
Change log
==========

Version 0.8.2
-------------
- Extension manual is now on docs.typo3.org, accessible
  via Typo3 extension repository.

Version 0.8.1
-------------

- Region entries containing paths will be truncated to 
  their basename.
- A wrong template for calendars had been shipped with the
  releases 0.7.6 and 0.8.0. This is corrected.
- Added Iceland.

Version 0.8.0
-------------

- Improved documentation.
- Repared plugin title visibilty in backend.

Version 0.7.6
-------------

- Added some further European countries (IT, SP, PT, LT, LI, EE).
- Added 12-24 and 12-31 as partials for Germany.
- Took provisions for displaying ISO 8601 week number.
- Took provisions for memorial days.
- Improved documentation for developers.

Version 0.7.5
-------------

- Changed TCA to prevent accidental editing of holidays table.
- Added shifting rule for events with fixed weekdays.

Version 0.7.4
-------------

- First publishing to Typo3 Extension Repository

Version 0.7.3
-------------

- Improved documentation.
- Added support for monthly repeated events.

Version 0.7.2
-------------

- Added choice of week start (Monday or Sunday). 
- Updated documentation.
- Fixed database record display in backend.

Version 0.7.1
-------------

- Initial public release.


