.. include:: ../Includes.txt

.. _introduction:

============
Introduction
============

What does it do?
================

LesPf Calendar displays a simple month calendar in a table. Common holidays 
are properly marked, you can also have markings for events you have entered 
into your Typo3 installation. The calendar can cope with one of several 
distinct event extensions. You may also configure a list of recurring days 
to be marked.

This extension's only purpose is a quick overview, it is not and shall not 
become a full calendar with all the bells and whistles.

The calendar can be styled by CSS.

.. _screenshots:

Screenshots
===========

Dependent on your CSS, the calendar might look like this.

.. figure:: ../Images/CalendarScreenshot.png
   :class: with-shadow
   :alt: Screenshot of Calendar Widget
   :width: 300px

   Calendar widget with holidays and events.
