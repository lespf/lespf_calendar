.. every .rst file should include Includes.txt
.. use correct path!

.. include:: Includes.txt

.. Every manual should have a start label for cross-referencing to
.. start page. Do not remove this!

.. _start:

=============================================================
Lespf Calendar
=============================================================

:Version:
   |release|

:Language:
   en

:Authors:
   Peter Willadt

:Email:
   willadt@t-online.de

:License:
   This extension documentation is published under the
   `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons)
   license

Calendar widget with holidays

.. important::

   Don't forget to set extension's version number in :file:`Settings.cfg` file,
   in the :code:`release` property.
   It will be automatically picked up on the cover page by the :code:`|release|` substitution.


**TYPO3**

The content of this document is related to TYPO3 CMS,
a GNU/GPL CMS/Framework available from `typo3.org <https://typo3.org/>`_ .

**Community Documentation**

This documentation is community documentation for the TYPO3 extension LesPf Calendar

It is maintained as part of this third party extension.

If you find an error or something is missing, please:
`Report a Problem <https://github.com/TYPO3-Documentation/TYPO3CMS-Example-ExtensionManual/issues/new>`__

**Extension Manual**

This documentation is for the TYPO3 extension lespf_calendar.

**For Contributors**

You are welcome to help improve this guide.

.. toctree::
   :maxdepth: 3

   Introduction/Index
   Editor/Index
   Installation/Index
   Configuration/Index
   Developer/Index
   KnownProblems/Index
   ChangeLog/Index
   Sitemap
