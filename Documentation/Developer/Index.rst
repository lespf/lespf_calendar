.. include:: ../Includes.txt

.. _developer:

================
Developer Corner
================

Calendar Formatting
===================

About CSS
---------

All HTML elements produced by the calendar are labeled with classes
starting with 'cal-'. You can style a calendar cell in different 
ways:

 - change background color
 
 - change font color
 
 - change font style

 - change border
 
This should be enough dimensions.To prevent that the quick overview 
the calendar should provide changes to a riddle, you probably
have to stick to a few distinct cases in general and to only one 
or two of these dimensions. Also you should keep accessibility in 
mind. As calendar entries may belong to several classes, you have to 
take CSS precedence rules into account to get the desired effect.

There is a sample CSS in the folder Resources/Public/CSS. This should
hopefully be sufficient as a starting point.

Week Start
----------

Support for start of the week on Monday (default, Europe) or Sunday (US)
is complete. If the week starts on Friday (middle east), the fluid 
template for week display has to be patched, in particular the title
row.

Calendar Week
-------------

You can access the ISO 8601 calendar week from your fluid template.
There is a sample fluid template WeeksLarge.html that shows how you can 
use it within your custom template. Please note that ISO 8601 calendar 
weeks only are useful if Monday is the start of the week. 

Adding Additional Calendars
===========================

Basically, there are two ways to enter calendar data:
 - YAML files
 - Entries in the table tx_lespfcalendar_domain_model_calendar
 
Your preferred way to enter data is to use a YAML file.
See below for the database.
You will find sample YAML files within the extension's
Resources/Private/Regions directory.

YAML files can be built completely from scratch,
they can include other YAML files or they can be based on the
database.

Please read the :ref:`configuration` section for the way to configure
the extension to find your YAML file. Please read 
the :ref:`for-editors` section to find out how to select a YAML 
file for the calendar widget.

The abbreviations of weekdays in calendar headings come from the
translation files. If your language is not supported yet and you do
not want to translate language files, you can configure the extension
to use your own fluid template with headings you have entered.

How date entries are built
--------------------------

Please inspect the YAMl files. They are commented and the common 
cases should be obvious. The meanings of fields are the same in
database entries. Just a few remarks:

When a *day* is specified, this date is fixed, so no rules apply.
When an *offset* is specified, the day will get calculated 
according to rules you may define. While a positive offset is 
identical to the day number, a negative offset counts from the 
end of the month, e.g. -1 is the last day. Specifying a weekday
(two letters, English) leeds to the day being moved, so offset 
8 and weekday TU means the second Tuesday in the month given.

Apart from months, offsets can be based on Easter, on solstices,
or on equinoxes. In YAML files, the keyword 'month' has to be
replaced by 'base' for this. Negative offsets count from the
date given. Easter, e.g. is in fact Easter Saturday, so
an offset of -1 results in Good Friday and an offset of 2 in
Easter Monday.

Shifting rules
--------------

Sometimes the day calculated is not appropriate, e.g. if it 
falls onto a Sunday or on a holidays. For this purpose shifting
rules exist.

There are two kinds of shifting:

1. Dates can be shifted if they happen to fall onto some unwanted 
   weekday. Shifting rules are specified by the weekday name (two
   letters, English), a colon, and the amount to shift. If more than 
   one rules is specified, they have to be separated by commas. 
   In YAML, the rules have to be quoted, as colons are special for 
   the YAML parser. Here is an example:

   .. code-block::

      shifting: 'SA: 2, SU: 1'
   
   When the day specified happens to fall on a Saturday, it will be
   moved to the following Monday. The same happens if it falls on a
   Sunday.

   These shifting rules are only considered if you have used an 
   offset (instead of a day) and you have not defined a weekday for 
   the offset given.

2. Dates can be shifted if they happen to fall onto some unwanted
   month day. These rules only get applied if you have used an 
   offset (instead of a day) and you have defined a weekday. They
   consist of the day number, a colon and the amount to shift.
   
   .. code-block::

      shifting: '3: 7, 28: -7'
   
   This example will shift an event from the third day of the given 
   month to the tenth and from the 28th day to the 21st. 
   
For each entry (holiday, event, what ever), there will only be one 
shifting rule applied and it will only be applied once. 
There are two reasons:

- the rules have to be easily understood, and
- accidental endless loops have to be prevented. It would be too
  easy to say something 'SU: 7' without thinking of the 
  consequences.

Configuring recurring events
----------------------------

The extension can process a YAML file instead or additional
to the internal database table. This gives you the chance to 
enter recurring events. There are sample YAML files in the 
extension's Resources/Private/Regions directory.

Important: You have to get your YAML right, or Typo3 might 
raise exceptions. Please check twice, especially when in doubt.

Memorial Days
-------------

Besides holidays and partials you can also enter memorial days.
There is, however, not much use to it, as long as the name of
the special day is not shown.

About the Table tx_lespfcalendar_domain_model_calendar
======================================================

The table contains official calendar data. It is intended that 
the table gets rebuilt from scratch whenever you reinstall or
update the calendar extension. So please don't enter any data 
there.

Extending the database table
----------------------------

The database table will probably be overwritten when you install
the next version of the calendar, so do not extend it. 
Please use YAML files instead.

I will happily include additional records. If you intend to supply 
calendar data to the extension, I prefer CSV data, but YAML is OK,
also.

The Data Model
--------------

The data model assumes a country that may contain a region. 
It is not intended to deal with sub regions or with constructs that
bundle several countries or several regions. This might save a few 
lines of data, but overviews (and, by the way, programming) would 
become more complicated.

Some Examples:

 - Germany has about 17 regions. If a holiday occurs in 
   ten of these regions, it has to be entered ten times (once for 
   each region).
   
 - France has regions and sub regions. If you decide to use sub regions
   (as I did), regions are not considered as parents to these
   sub regions, but as separate entities.

.. _developer-api:

