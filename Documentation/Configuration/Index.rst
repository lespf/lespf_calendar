.. include:: ../Includes.txt

.. _configuration:

=============
Configuration
=============

Please include the LesPF Calendar extension template on the root page or on 
the page where you want to use the plugin.

You will probably also include some CSS. Either the sample CSS that comes
with the extension, or you write your own, which is probably more appropriate.

The general configuration is handled within Typo3's *extension configuration*
module. 

As the week starts on Monday in some countries and on Sunday in others, 
you may choose the appropriate day.

You also decide, which (if any) event provider is to be used and which
categories shall be referenced. 

By now, the following event providers are supported:
 - Eventnews
 - Sfeventmgt
 - News
 
The support for *News* is incomplete, as the News extension does not support 
the end of an event, so  events entered in news we'll always appear as events
spanning only one day. 

.. figure:: ../Images/CalendarConfigurationGeneral.png
   :class: with-shadow
   :alt: Configuring the event source.
   :width: 300px

   Configuring start of the week (1 for Europe, 7 for US) and event source. 
   Choose your event extension (or *none*, if none).

For the categories, you type in a comma-separated list of category numbers 
and choose how to handle them. You got the following options:

 - include: Only the listed categories will be taken into account.
 - exclude: Events belonging to the listed categories will not appear 
   in the HTML output, but if they belong to other categories too, they will.
 - exclude (strict): Events belonging to the listed will not appear in the 
   HTML output, even if they also belong to other categories.
 - ignore: All events will be listed, your category list (if any) 
   does not matter.

.. figure:: ../Images/CalendarConfigurationCategories1.png
   :class: with-shadow
   :alt: Configuring categories (general).
   :width: 300px

   Configuring categories: Here anything is allowed.

In the backend, where a calendar widget is used, you choose the month to
display (leave year and month empty to show the current month) and you choose
the region to display holidays. You can also choose a starting page for events
to be considered. If you leave it empty, all events will be parsed.

The region has to be entered as the two digit ISO country code, followed by a 
hyphen and the ISO region code. If you only enter the country code, only 
holidays that are valid in the complete country will be displayed. 
Examples are DE-BY (Bavaria), GB-SCT (Scotland) of FR (France in general).

Unfortunately, the list of regions is far from complete. I will happily 
include more countries and regions. 

Extending data
==============

You may adopt further countries or regions in the form of YAML files. 
Sample files are provided in the Resources/Private/Regions subdirectory 
of the extension. You can also add other kinds of recurring events than 
holidays to your YAML file. You will find more information in the 
:ref:`developer` developer section of this manual. 

To make the extension use your YAML file, you have to set the TypoScript 
variable ext.lespf_calendar.settings.customeventpath to an appropriate value, 
e.g. *fileadmin* and you have to add .yaml to the region code in the plugin 
configuration, e.g. "custom.yaml" if your file happens to be named so. Please
cosult the4 next section for security concenrs.


Privacy and Security Concerns
=============================

Events
------

Displaying events in the calendar may circumvent restrictions on the 
visibility of these events in the frontend of your site. This can be 
mitigated by excluding several categories from the calendar or by having 
your events within distinct folders and restricting the calendar's access 
to some of them. 

To be clear: general restrictions (publishing date, deletion, etc.) are 
respected and there is not more to see than that there is an event with some 
category number on some day.

Categories
----------

Categories on the other hand are a feature of Typo3 that is only accessible in
the backend. The calendar uses (and thereby leaks) category numbers within its 
CSS classes. You have to decide if this is acceptable to you. As the choice of 
event provider and categories to include is handled within the extension
configuration, editors can not tamper with restrictions given by the
administrator.

YAML files
----------

Malformed YAML files may leed to error messages during calendar generation.
You may want to restrict the path to YAML files to a location where not
every backend user has write access. The best place my be inside of your site
package or in a subfolder of fileadmin with access restrictions.


Typical Example
===============

Typoscript Template
-------------------

1. Configure the calendar within the extension configuration menu.
2. Include the Lespf Calendar Template.
3. Include CSS within the *setup* section (mandatory) and configure the path 
   to custom calendars (optional, only if you need them).

.. code-block:: typoscript

   plugin.tx_lespf_calendar.settings {
      # Optional: configure path for private YAML file
      # If restricting backend users is not important:
      #   customeventpath = fileadmin/
      # else (probably better) something like:
      customeventpath = EXT:my_extension/Resources/Private/Regions/
   }
   page {
     includeCSS {
       myCustomStylesheet = EXT:lespackage/Resources/Public/Css/csspatch.css
       myOtherStyleSheet = EXT:lespf_calendar/Resources/Public/Css/lespf_calendar.css
     }
   }
   
Restricting Categories
----------------------

.. figure:: ../Images/CalendarConfigurationCategories2.png
   :class: with-shadow
   :alt: Configuring categories (example).
   :width: 300px

   Configuring Categories: Set some restrictions.
   
Getting Category Numbers
------------------------

When writing CSS, you need category numbers. You may retrieve them from HTML output,
but you can just as easily query the database (if you got an appropriate tool and
the necessary privileges).

.. code-block:: sql

   SELECT uid, title FROM `sys_category` WHERE deleted = 0;

If you got categories with start time or end time, you perhaps have to do 
some more work to get an accurate list.
