.. include:: ../Includes.txt

.. _installation:

============
Installation
============

LesPf calendar can be installed like other extensions, by TER, 
by ZIP file, or by Composer. See also  :ref:`t3install:start`.

For composer users, the command line is

.. code-block:: sh

  composer require lespf/lespf-calendar

LesPf calendar comes with extra data to be inserted into the 
database. This data is contained within the file 
ext_tables_static+adt.sql in the extension's root directory. 
Unfortunately, Typo3 does import this file only on the first 
install of the extension. As by now there is no update mechanism 
for this, please load this data with an external tool (e.g. the 
import module of PHPmyadmin), when doing a repeated install.

Dependencies
============

LesPf calendar needs the PHP datetime extension. This extension 
is in typically incuded in hosted environments. If you run your
own server, you may perhaps have to install it. 

As long as you are happy with weeks and holidays, there are no 
further dependencies. If you want to display events, you have to 
use an extension that supports this. LesPf Calendar can read data 
from the following extensions:

- SfEventMagement, 
- Eventnews, 
- or from News. 

You may install one of them after LesPf Calendar, if you have not 
done so before.
