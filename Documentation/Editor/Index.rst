.. include:: ../Includes.txt

.. _for-editors:

===========
For Editors
===========

Choosing a Region
=================

The calendar plugin gets included like any other plugin. You have to enter a region
for proper display of holidays. Normally, region names consist of a country name 
and an (optional) region name, separated by a hyphen. Please use uppercase, 
according to ISO 3166.

If a region does not work, it may be not yet contained in the region database.

If your admin has configured a special file with additional dates, you have to 
enter the file name with extension .yaml instead of a region code. 
So instead oy typing something like 'GB-SCT' or 'FR', you enter 'custom.yaml'
or something like that.

.. figure:: ../Images/CalendarCalendarWidgetBackend.png
   :class: with-shadow
   :alt: Configuring the calendar widget.
   :width: 300px

   Configuring the calendar widget.

List of supported Regions
-------------------------

This list is due to version 0.8.1 of the calendar extension.
Please visit the change log :ref:`changelog` for eventual further additions.

.. list-table:: Regions
   :widths: 15, 50
   :header-rows: 1
   
   * - Country
     - Regions
   * - AT
     - 1 to 9
   * - BE
     - VLG, WAL[1]_
   * - CZ
     -
   * - DE
     - BB, BE, BW, BY, HB, HH, MV, NI, NW, RP, SH, SL, SN, ST, TH
   * - DK
     -
   * - EE
     -
   * - FI
     -
   * - FR
     - 67, 67, 68[2]_
   * - GB
     - ENG, NIR, SCT, WLS
   * - IS
   * - IT
     -
   * - LI
     -
   * - LT
     -
   * - LU
     -
   * - NL
     -
   * - PL
     -
   * - PT
     -
   * - SE
     -
   * - SK
     -
   * - SP
     - [1]_


.. [1] Somewhat incomplete, please read :ref:`known-problems`
.. [2] For all other departments just use 'FR'.
   
Filtering Events
================
   
If you got events that should not be visible to website users, you have to do
something more:

 - If you have distinct folders for public and private events, choose a folder
   (or several folders) in which you have entered the public events.
   If you do not choose at least one folder, any folder will be considered.
   
 - You may give a special category to any event that shall be displayed or hidden.
   Your administrator can configure the categories to display.
   
Using distinct folders is probably the better way, as it is more obvious to choose
a folder to store an event than to think of setting categories anytime you create 
an event.

How to find out category numbers
================================

The easy way without any database privileges is to tag an event with some 
categories and to view the calendar output source code in your browser.

Including Changed Events
========================

The calendar gets redrawn with Typo3's cache cycle (probably once a day). If you 
have changed or created an event within the current month, you should clear the 
cache for the page on which the calendar is shown, so that it gets immediately
visible.
