.. include:: ../Includes.txt

.. _known-problems:

==============================
Limitations and Known Problems
==============================

Updating
========

Automatic reimport of calendar base data does not happen, when the
extension gets reinstalled. With traditional installations, you just
can reimport data with the extension manager.

Calendars
=========

The calendar plugin is based on the Gregorian Calendar, so it fits 
European countries well, but probably nothing else. It defaults to 
Sunday being the last day of the week, but it can be tweaked to 
accept Sunday as the first weekday.

There is no provision or intention to handle other calendar systems, 
e.g. the lunar calendar of Islam or the lunisolar Jewish calendar.

Holidays
========

I have little understanding of holidays outside my cultural bubble.
Beside a lack of time, this lack of expertise keeps me from adding
all the holiday lists I can find.

Just some examples for difficulties:

 - In Denmark there are two holidays that always happen
   on Sunday. In Germany, I would not include such days into a calendar.
 - In Bavaria, there is a holiday that is only valid in communities with
   a majority of catholic people. This can not be properly shown.
   In cases like this, you should add it to your site as an event or
   within a YAML file.
 - In Austria, not nation-wide holidays appear to be only valid for
   the public sector, but not for businesses. I tried to map this by
   calling these days *partial* instead of *holiday*.
 - In Germany, Dec. 24 and Dec. 31 have special rules for opening
   hours of stores, but these days are not holidays. I have not included
   them, though they might also be partials.
 - In the UK, holidays get shifted to the next Monday if they happen
   to fall on a weekend. I tried to map this by the *shifting...* 
   field.
 - In the Czech Republic, there exist two kinds of holidays that 
   apparently differ by the categories of stores that are allowed to 
   open. I have flagged both kinds as holiday.
 - Also in the Czech Republic, there are official remarkable days that 
   are apparently no holidays. I have not entered them.
 - In Belgium, there exist holidays for the Flemish part of the country, 
   for the Walloon part, and for the German part. Unfortunately, the 
   ISO 3166-2 region codes know only about the Flemish part, the Walloon
   part and the Brussels region. So for me, there is one region missing
   and one region that I do not know how to handle.
 - In Switzerland, there are at least four distinct levels of holidays,
   the level varying from region to region, and there are a lot of local 
   holidays. Also it has to be taken into account, if a majority of the
   population is catholic. Oh, and some holidays should get canceled if
   it leads to Monday and Tuesday being a holiday, but this cancelling
   is not respected everywhere[1]. 
   As a consequence, I feel not qualified 
   to enter Swiss holidays correctly. 
   
.. [1] There is no special command for deleting a date. You can handle
   this if you build a shifting rule that changes the year 
   (e.g. shifting: \'TU: 365\').
      
Events
======

Recurring events within YAML files that span from the end of one year to 
the next will not show up in December and January. Please split them up.

There is no rule for shifting recurring events away from holidays.

Invisible Events
----------------

You will probably define Typo3 access rules for events. The calendar
respects these rules according to the current date. So, if you have an
event that is not visible any more (as it happened in the past or as you 
have deleted it), it will not be visible in the calendar, too. Of course, 
this applies also to future events.
   
YAML Trouble
------------

I decided to use YAMl for user-generated events and holidays, because 
there is a YAML parser built into Typo3. Personally, I do not like YAML.
But writing JSON or XML by hand is also not great fun.

You have to get your YAML right, or Typo3 might raise exceptions.
Please check twice when in doubt. If you don't trust your editors for
being able to write correct YAML, you can restrict the custom event path
to a folder where they can not write.

Implementation
==============

The calendar extension uses raw database queries to determine events.
This is not generally considered as good practice, but it is way faster
than to load complete events, just to find out when they start and end.




