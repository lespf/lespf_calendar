<?php

declare(strict_types=1);

namespace Lespf\LespfCalendar\Tests\Unit\Controller;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Test case
 *
 * @author Peter Willadt <willadt@t-online.de>
 */
class CalendarControllerTest extends UnitTestCase
{
    /**
     * @var \Lespf\LespfCalendar\Controller\CalendarController|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder($this->buildAccessibleProxy(\Lespf\LespfCalendar\Controller\CalendarController::class))
            ->onlyMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllCalendarsFromRepositoryAndAssignsThemToView(): void
    {
        $allCalendars = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $calendarRepository = $this->getMockBuilder(\::class)
            ->onlyMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $calendarRepository->expects(self::once())->method('findAll')->will(self::returnValue($allCalendars));
        $this->subject->_set('calendarRepository', $calendarRepository);

        $view = $this->getMockBuilder(ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('calendars', $allCalendars);
        $this->subject->_set('view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenCalendarToView(): void
    {
        $calendar = new \Lespf\LespfCalendar\Domain\Model\Calendar();

        $view = $this->getMockBuilder(ViewInterface::class)->getMock();
        $this->subject->_set('view', $view);
        $view->expects(self::once())->method('assign')->with('calendar', $calendar);

        $this->subject->showAction($calendar);
    }

    /**
     * @test
     */
}
