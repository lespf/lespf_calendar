<?php

declare(strict_types=1);

namespace Lespf\LespfCalendar\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 * @author Peter Willadt <willadt@t-online.de>
 */
class CalendarTest extends UnitTestCase
{
    /**
     * @var \Lespf\LespfCalendar\Domain\Model\Calendar|MockObject|AccessibleObjectInterface
     */
    protected $subject;

   protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \Lespf\LespfCalendar\Domain\Model\Calendar::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getMonthReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getMonthFrom()
        );
    }

    /**
     * @test
     */
    public function setMonthForIntSetsMonth(): void
    {
        $this->subject->setMonthFrom(12);

        self::assertEquals(12, $this->subject->_get('monthFrom'));
    }

    /**
     * @test
     */
    public function getYearReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getYearFrom()
        );
    }

    /**
     * @test
     */
    public function setYearForIntSetsYear(): void
    {
        $this->subject->setYearFrom(12);

        self::assertEquals(12, $this->subject->_get('yearFrom'));
    }
}
