<?php
defined('TYPO3') || die();

(static function() {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'LespfCalendar',
        'Calendarwidget',
        [
            \Lespf\LespfCalendar\Controller\CalendarController::class => 'list, show'
        ],
        // no non-cacheable actions
        [
        ]
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    calendarwidget {
                        iconIdentifier = lespf_calendar-plugin-calendarwidget
                        title = LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_be.xlf:calendarplugin.title
                        description = LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_be.xlf:calendarplugin.description
                        tt_content_defValues {
                            CType = list
                            list_type = lespfcalendar_calendarwidget
                        }
                    }
                }
                show = *
            }
       }'
    );
})();
