<a name="readme-top"></a>

<!--
HOW TO USE:
This is an example of how you may give instructions on setting up your project locally.

Modify this file to match your project and remove sections that don't apply.

REQUIRED SECTIONS:
- Table of Contents
- About the Project
  - Built With
  - Live Demo
- Getting Started
- Authors
- Future Features
- Contributing
- Show your support
- Acknowledgements
- License

OPTIONAL SECTIONS:
- FAQ

After you're finished please remove all the comments and instructions!
-->

<!-- TABLE OF CONTENTS -->

# 📗 Table of Contents

- [📖 About the Project](#about-project)
  - [🛠 Built With](#built-with)
    - [Tech Stack](#tech-stack)
    - [Key Features](#key-features)
  - [🚀 Live Demo](#live-demo)
- [💻 Getting Started](#getting-started)
  - [Setup](#setup)
  - [Prerequisites](#prerequisites)
  - [Install](#install)
  - [Usage](#usage)
  - [Run tests](#run-tests)
  - [Deployment](#deployment)
- [👥 Authors](#authors)
- [🔭 Future Features](#future-features)
- [🤝 Contributing](#contributing)
- [🙏 Acknowledgements](#acknowledgements)
- [❓ FAQ](#faq)
- [📝 License](#license)

<!-- PROJECT DESCRIPTION -->

# 📖 Lespf_Calendar <a name="about-project"></a>

Lespf Calendar is a plugin extension for the Typo3 CMS. It shows a 
tiny month calendar with markers for holidays and - if you wish - events.

![Screenshot of calendar](/Documentation/Images/CalendarScreenshot.png "A typical screenshot")
     
## 🛠 Built With <a name="built-with"></a>

### Tech Stack <a name="tech-stack"></a>

The main language is PHP. There is some use of fluid templates and CSS.
Data is handled in form of YAML files (local) and SQL queries (global).

<details>
  <summary>Server</summary>
  <ul>
    <li><a href="https://typo3.org/">Typo3</a></li>
    <li>PHP from 8.1 on</li>
  </ul>
</details>

<details>
<summary>Database</summary>
  <ul>
    <li>MariaDB or other database supported by Typo3</li>
  </ul>
</details>

<!-- Features -->

### Key Features <a name="key-features"></a>

- holidays for many European countries are included
- events can be imported from a common event extension
- styling by CSS

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need:

- A webserver with PHP from version 8.0 with Datetime extension,
- Typo3 V11 or V12, and
- if you want to import events from extensions, one of the 
  following plugins:
  - News,
  - EventNews, or
  - SfEventMgt

### Setup

As long as the calendar is neither on packagist nor on the Typo3 
extension repository (TER), you have to download it manually.

So either clone this repository to your desired folder:
  sh
  cd your-folder
  git clone willadt@gitlab.com:lespf/lespf_calendar.git
or download it as a ZIP file.

### Install

Install this project with:

- composer, or
- upload it with the Typo3 extension manager.

For composer, you have to configure your Typo3 installation for local
packages and put the files there, if you want to get the cutting edge.
If the latest official version is OK, you just can emit

    composer require lespf/lespf-calendar

For upload of a cutting edge version with the extension manager into your
Typo3 installation, you should rename the ZIP file 
that you downloaded to lespf_calendar.zip (removing the
release date/number stuff) before uploading. Official numbered 
versions should find their way to TER, so you may download them from
there.

## Post-Install

Run Maintainance/database compare after installation.
When reinstalling, you have to reload the database table. You might 
want to use a tool like phpmyadmin to import ext_tables_static+adt.sql,
if database upload within the extension manager does not suffice.


### Usage

There is (hopefully complete) documentation for configuration and usage 
of the plugin written in Restructured Text format within the project's
Documentation subfolder. It shall not be repeated here.

This documentation is also accessible online via the Typo3 
extension repository (TER).

For writing YAMl files please stick to the examples in the subfolder
Resources/Private/Regions.


<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- AUTHORS -->

## 👥 Authors <a name="authors"></a>

👤 **Peter Willadt**

- GitLab: [@gitlabhandle](https://gitlab.com/willadt)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- FUTURE FEATURES -->

## 🔭 Future Features <a name="future-features"></a>

- [ ] **add further countries**

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## 🤝 Contributing <a name="contributing"></a>

Contributions, issues, and feature requests are welcome!

As holidays are best understood if they happen in the culture you live in,
I am grateful for help. Within the package documentation you
will find some examples of difficulties I was confronted with while
scratching on the surface.

Feel free to check the [issues page](../../issues/).

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ACKNOWLEDGEMENTS -->

## 🙏 Acknowledgments <a name="acknowledgements"></a>

I would like to thank...

- the Typo3 community for developer documentation
- the authors of several plugins I inspected for the tricks of the trade, 
  especially 'static Info Tables' and 'tt_address'

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- FAQ -->

## ❓ FAQ<a name="faq"></a>

- **How can  find out which countries are supported?**

  - As the list of countries grows from time to time, 
    the safest way is to inspect the file ext-tables+adt.sql

- **Can I add private entries to the database table?**

  - Please don't. They would be gone with the next update.
    You may add your holidays (and events) with YAML files.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- LICENSE -->

## 📝 License <a name="license"></a>

As a Typo3 extension,
this project is licensed accoding to the GNU General Public License, version 2 or later.
Please consult license information for the Typo3 project for details.

<p align="right">(<a href="#readme-top">back to top</a>)</p>
