<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel',
	'label' => 'holidayname',
        'label_alt' => 'country, region, holidayname',
        'adminOnly' => 1,
        'is_static' => 1,
        'readOnly' => 1,
        'rootLevel' => 1,
        'default_sortby' => 'country, region, base',
        'enablecolumns' => [
#            'disabled' => 'hidden',
#            'starttime' => 'starttime',
#            'endtime' => 'endtime',
        ],
        'searchFields' => 'country,region,holidayname',
        'iconfile' => 'EXT:lespf_calendar/Resources/Public/Icons/Extension.svg'
    ],
    'types' => [
        '1' => ['showitem' => 'country, region, specialyear, base, monthday, distance, weekday, shifting, kind, holidayname'],
    ],
    'columns' => [
        'country' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel.country',
            'config' => [
                'type' => 'input',
                'size' => 2,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'region' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel.region',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'specialyear' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel.specialyear',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 0
            ]
        ],
        'base' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel.base',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'monthday' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel.monthday',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 0
            ]
        ],
        'distance' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel.distance',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 0
            ]
        ],
        'weekday' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel.weekday',
            'config' => [
                'type' => 'input',
                'size' => 2,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'shifting' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel.shifting',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'default' => ''
            ]
        ],
        'kind' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel.kind',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'holidayname' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lespf_calendar/Resources/Private/Language/locallang_db.xlf:tx_lespfcalendar_domain_model_calendarmodel.holidayname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
    
    ],
];

