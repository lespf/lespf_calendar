<?php

defined('TYPO3') || die();

$pluginSignature = 'lespfcalendar_calendarwidget';

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
# $pluginSignature =
  \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'LespfCalendar',
    'calendarwidget',
    'Calendar Widget'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:lespf_calendar/Configuration/FlexForms/CalendarPlugin.xml'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'pages,recursive';
