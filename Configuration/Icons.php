<?php

return [
    'lespf_calendar-plugin-calendarwidget' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:lespf_calendar/Resources/Public/Icons/user_plugin_calendarwidget.svg'
    ],
];
